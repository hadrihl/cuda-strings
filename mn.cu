// author: hadrihl <hadrihilmi@gmail.com>
#include <stdio.h>
#include <cuda.h>

struct mystring {
	char str[6];
};

__device__ char* my_strcpy(char* dest, const char* src) {
	int i = 0;
	do {
		dest[i] = src[i];
	} while (src[i++] != 0);

	return dest;
}

__global__ void kern(mystring* d_a) {
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	
	printf(">>>device: %s\n", d_a[idx].str);
	my_strcpy(d_a[idx].str, "C");
	printf(">>>device: %s\n", d_a[idx].str);
}

int main(int argc, char* argv[]) {
	mystring* h_a;
	const int N = 5;

	h_a = (mystring*)calloc(N, sizeof(mystring)); int i;
	for(i = 0; i < N; i++) { strcpy(h_a[i].str, "-A-"); printf("%s\n", h_a[i].str); }

	mystring* d_a; size_t cudaSize = N * sizeof(mystring);
	cudaMalloc((void**) &d_a, cudaSize);
	
	cudaMemcpy(d_a, h_a, cudaSize, cudaMemcpyHostToDevice);

	kern<<< 1, N >>>(d_a);

	cudaError_t err = cudaMemcpy(h_a, d_a, cudaSize, cudaMemcpyDeviceToHost);
	if(err != cudaSuccess) { printf("err: %s\n", cudaGetErrorString(err)); exit(-1); }
	
	cudaFree(d_a);

	printf("\n\n"); for(i = 0; i < N; i++) printf("%s\n", h_a[i].str);

	return 0;
}
