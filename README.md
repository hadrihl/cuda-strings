cuda-strings
============
A simple strings manipulation in CUDA.

Compile
-------
$ nvcc mn.cu -arch=sm_20 -o a.out

Run
---
$ ./a.out

Author
------
hadrihl // hadrihilmi@gmail.com

